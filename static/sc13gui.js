/**
 *  SC13 Demo GUI module.
 */
var sc13gui = sc13gui || (function() {
	// link styles. true=link is in the path
	var link_colors = {false: "#999", true: "#0c0"};
	var link_width  = {false: "2px",  true: "4px"};
	var link_dash   = {false: "4,4",  true: "0,0"};

	var kwh_rates	= {};
	var energy_mixes = {};
	var extra		= {};

	// get the canvas the topology will be rendered on.
	var svg = d3.select("#cloud")
		.style("border", "1px solid black");

	var width  = svg.attr("width");
	var height = svg.attr("height");

	var color = d3.scale.category10();

	var node_colors = {
		true  : "#1f77b4", //color(0),
		false : "#999" //color(1)
	}

	// list of svg elements that make up the graph.
	var svg_links = [],
		svg_nodes = [],
		svg_links_labels = [],
		svg_nodes_labels = [];

	// g elements the graph elements are contained in.
	var g_links = svg.append("g");
	var g_nodes = svg.append("g");

	var tooltip = d3.select("body").append("div")
		.attr("class", "tooltip")
		.style("opacity", 0);

	// force layout
	var force = d3.layout.force()
		.charge(-800)
		.linkDistance(2)
		.size([width, height]);

	// clamp x pos with radius to width
	function clampw(x, r) {
		return Math.max(r, Math.min(width - r, x));
	}

	// clamp y pos with radius to height
	function clamph(y, r) {
		return Math.max(r, Math.min(height - r, y));
	}

	// this function is called on every tick and updates
	// the position and style of all the elements.
	force.on("tick", function() {
		svg_links
			.attr("x1", function(d) { return d.source.x; })
			.attr("y1", function(d) { return d.source.y; })
			.attr("x2", function(d) { return d.target.x; })
			.attr("y2", function(d) { return d.target.y; })
//			.transition()
//			.duration(200)
			.attr("stroke-width",	  function(d) { return link_width[d.inpath]; })
			.attr("stroke-dasharray", function(d) { return link_dash[d.inpath]; })
			.attr("stroke",		      function(d) { return link_colors[d.inpath]; });

/*
		svg_links_labels
			.attr("x", function(d) { return (d.source.x + d.target.x) / 2; } )
			.attr("y", function(d) { return (d.source.y + d.target.y) / 2; } );
*/

		svg_nodes_labels
			.attr("x", function(d) { return d.x; })
			.attr("y", function(d) { return d.y; });

		svg_nodes
			.attr("cx", function(d) { return clampw(d.x, 8); })
			.attr("cy", function(d) { return clamph(d.y, 8); });
	});

	// this function is attached to the graph nodes
	// so that they can be dragged and stay fixed
	// after having been dragged.
	var drag = force.drag()
		.on("dragstart", function(d) {
			d.fixed = true;
			d3.select(this).classed("fixed", true);
		});

	// init the graph and data
	function initialize(json) {
		kwh_rates = json.rates;
		energy_mixes = json.mixes;
		extra = json.extra;
		force
			.nodes(json.nodes)
			.links(json.links);
		render();
		force.start();
	}

	// update the links
	function update(json) {
		extra = json.extra;
		force
			.links(json.links);
		render();
		force
			.start()
			.alpha(0.01);
	}

	// draw the graph
	function render() {
		svg_links = g_links.selectAll("line")
			.data(force.links());

		svg_links.enter().append("line");
	//		.attr("class", "link")
	//		.attr("stroke-width",	 function(d){ return link_width[d.inpath]; })
	//		.attr("stroke-dasharray", function(d){ return link_dash[d.inpath]; })
	//		.attr("stroke",		   function(d){ return link_colors[d.inpath]; });

/*
		svg_links_labels = g_links.selectAll("text")
			.data(force.links());

		svg_links_labels.enter().append("text")
			.text(function(d) { return d.rtt; } )
			.attr("class", "link")
			.attr("fill", "#111")
			.attr("text-anchor", "middle");
*/

		svg_nodes = g_nodes.selectAll("circle")
			.data(force.nodes());

		svg_nodes.enter().append("circle")
			.attr("class", "node")
			.attr("r", 8)
			// color
			.style("fill", function(d) {
				return node_colors[d.endpoint];
			})
			.call(drag)
			// tooltip mouseover
			.on("mouseover", function (d) {
				var x = extra.d[d.name];
				var u = extra.u[d.name];
				var region = d.region;
				var pdyn = d.power_dynamic;
				var pwr = d.power_static + pdyn * (1 - x) + pdyn * x * u;
				var text =
					d.type + "<br/>" +
					"Utilization: " + Math.round(u * 100, 0) + "&#37;<br/>" + 
					"Power: " + Math.round(pwr * 1000) / 1000.0 + " kW<br/>" +
					"kWh rate: " + kwh_rates[region] + " cents<br/>" + 
					"Emissions: " + Math.round(energy_mixes[region], 0) + " gr. CO2/kWh<br/>";
				tooltip.transition()
					.duration(200)
					.style("opacity", 0.9);
				tooltip.html(text)
					.style("left", (d3.event.pageX) + "px")
					.style("top",  (d3.event.pageY) + "px");
			})
			// tooltip mouseout
			.on("mouseout", function (d) {
				tooltip.transition()
					.duration(500)
					.style("opacity", 0);
			})
			// tooltip mousemove
			.on("mousemove", function(d) {
				tooltip
					.style("left", (d3.event.pageX) + "px")
					.style("top",  (d3.event.pageY) + "px");
			});

		svg_nodes_labels = g_nodes.selectAll("text")
			.data(force.nodes());

		svg_nodes_labels.enter().append("text")
			.text(function(d) { return d.name; })
			.attr("class", "node")
			.attr("text-anchor", "middle")
			.attr("dy", "-10px");

		svg_links.exit().remove();
//		svg_links_labels.exit().remove();
		svg_nodes.exit().remove();
		svg_nodes_labels.exit().remove();
	}

	// public interface
	return {
		"initialize": initialize,
		"update": update,
		"render": render
	};
})();
