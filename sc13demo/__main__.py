import networkx as nx
import gpce
import ujson as json
import os
import random
import time
from flask import Flask, Response, request,\
	abort, send_from_directory, render_template, redirect

def __read_energy(path):
	with open(path) as f:
		data = json.loads(f.read())
	kwh_rates = data['kwh_rates']
	emissions = gpce.CarbonEmissions(data['carbon_emissions'])
	energy_mixes = data['energy_mixes']
	energy_mixes = dict(
		(k, emissions(v.iteritems())) for k,v in energy_mixes.iteritems() )
#	for k,v in energy_mixes.iteritems():
#		print k,v
	return kwh_rates, energy_mixes

def __read_topology(path):
	with open(path) as f:
		data = json.loads(f.read())
	nodes = [ gpce.Node(k, **v) for k,v in data['nodes'].iteritems() ]
	edges = [ gpce.Edge(s, d, **a) for s,d,a in data['edges'] ]
	util = data.get('utilization', 1.0)
	if data.get('bidirectional', True):
		graph = nx.Graph()
	else:
		graph = nx.BiGraph()
	graph.add_nodes_from(nodes)
	graph.add_edges_from(edges)
	return graph, util

def __read_config():
	cfgfile = os.getenv('SC13CONF', None)
	if not cfgfile:
		raise Exception('SC13CONF environment variable not given! Cannot start service.')
	with open(cfgfile) as f:
		config = json.loads(f.read())
	topology_file = config.get('topology', 'topology.json')
	energy_file = config.get('energy', 'energy.json')
	home = os.path.abspath( config.get('home', '') )
	if not home:
		home = os.path.abspath(os.path.dirname(__file__))
	config['home'] = home
	kwh_rates, energy_mixes = __read_energy( os.path.join(home, energy_file) )
	graph, util = __read_topology( os.path.join(home, topology_file) )
	pathcalc = gpce.PathCalculator(graph, energy_mixes, kwh_rates, util)
	return config, pathcalc

config, pathcalc = __read_config()
app = Flask('SC13 Green Path Computation Demo')

##
##
##

## just redirect to the gui
@app.route('/')
def root():
	return redirect('/v1/gui')

## static files
@app.route('/static/<path:filename>')
def serve_static(filename):
	if '..' in filename or filename.startswith('/'):
		abort(404)
	return send_from_directory('./static', filename)

## energy data
@app.route('/v1/energy')
def get_energy():
	path = os.path.join(config['home'], 'energy.json')
	return Response(open(path), mimetype='application/json')

## topology data
@app.route('/v1/topology')
def get_topology():
	path = os.path.join(config['home'], 'topology.json')
	return Response(open(path), mimetype='application/json')

## calculate paths from src to dst
@app.route('/v1/path')
def get_path():
	src    = request.args.get('src', None)
	dst    = request.args.get('dst', None)
	orderby = request.args.get('orderby', 'gbits')
	loss   = float(request.args.get('loss', 10**-7))
	mss    = int(request.args.get('mss', 8840))
	c      = float(request.args.get('c', 1.0))

	if (src == dst) or (orderby not in pathcalc.get_metrics()):
		abort(400)
	try:
		paths = pathcalc.compute_paths(src, dst, orderby, loss, mss, c)
	except nx.exception.NetworkXError, e:
		return Response(e, status=400, mimetype='text/plain')
	content = json.dumps(paths)
	return Response(content, mimetype='application/json')

## given a path, calculate the metrics
@app.route('/v1/path/metrics')
def get_path_metrics():
	path = json.loads(request.args.get('path'))
	loss = float(request.args.get('loss', 10**-7))
	mss  = int(request.args.get('mss', 8840))
	c    = float(request.args.get('c', 1.0))

	try:
		x = dict(
			gbits     = pathcalc.path_gbits(path, loss, mss, c),
			gramsco2  = pathcalc.path_gramsco2(path, loss, mss, c),
			usdcents  = pathcalc.path_usdcents(path, loss, mss, c),
			kwh       = pathcalc.path_kwh(path, loss, mss, c),
		)
	except KeyError, e:
		return Response('node %s does not exist' % str(e)[2:-1], status=400, mimetype='text/plain')
	content = json.dumps(x)
	return Response(content, mimetype='application/json')

##
## The following urls are purely for the GUI.
##

@app.route('/v1/gui')
def get_gui():
	nodes = pathcalc.endpoints #nodes()
	nodes.sort()
	return render_template('sc13gui.html', nodes=nodes)

@app.route('/v1/gui/topology')
def get_gui_topology():
	g = pathcalc.graph
	nodes = g.nodes()
	edges = g.edges()
	mk_node = lambda n: dict(
		name=n,
		inpath=False,
		group=nodes.index(n),
		**g.node[n])
	mk_link = lambda s, d: dict(
		source=nodes.index(s),
		target=nodes.index(d),
		inpath=False,
		**g.edge[s][d])
	links = [mk_link(s, d) for s,d in edges]
	nodes = [mk_node(n) for n in nodes]
	content = json.dumps({
		'nodes' : nodes,
		'links' : links,
		'mixes' : pathcalc.energy_mixes,
		'rates' : pathcalc.kwh_rates,
		'extra' : {
			'u' : __random_dist(g.nodes(), '100', 0, 0, 0.0),
			'd' : __random_dist(g.nodes(), '100', 0, 0, 0.0),
		},
	})
	return Response(content, mimetype='application/json')

def __best_paths(paths):
	def sort_paths(metric, reverse):
		return sorted(paths, reverse=reverse, key=lambda k: k[1][metric])
	metrics = [
		('fastest', 'gbits', True),
		('greenest', 'gramsco2', False),
		('cheapest', 'usdcents', False),
		('leastpwr', 'kwh', False),
		('shortest', 'hops', False),
	]
	return dict( (k, sort_paths(m, r)[0][1]) for k,m,r in metrics)

random_dists = {
	'0' : lambda a, b: 0.0,
	'1' : lambda a, b: 0.01,
	'10' : lambda a, b: 0.1,
	'25' : lambda a, b: 0.25,
	'50' : lambda a, b: 0.5,
	'75' : lambda a, b: 0.75,
	'100' : lambda a, b: 1.0,
	'uniform' : lambda a, b: random.uniform(0.0, 1.0),
	'beta' : random.betavariate,
}

def __random_dist(keys, fn, a, b, m):
	f = random_dists[fn]
	def fx():
		x = f(a, b)
		return x + (1.0 - x) * m
	return dict( (k, fx()) for k in keys )

@app.route('/v1/gui/path')
def get_gui_path():
	src    = request.args.get('src', None)
	dst    = request.args.get('dst', None)
	orderby = request.args.get('orderby', 'gbits')
	loss   = float(request.args.get('loss', 10**-7))
	mss    = int(request.args.get('mss', 8840))
	c      = float(request.args.get('c', 1.0))
	nth    = int(request.args.get('nth', 0))
	urdist = request.args.get('urdist', '100')
	ua     = float(request.args.get('ua', 2.0))
	ub     = float(request.args.get('ub', 2.0))
	um     = float(request.args.get('um', 0.0))
	drdist = request.args.get('drdist', '0')
	da     = float(request.args.get('da', 2.0))
	db     = float(request.args.get('db', 2.0))
	seed   = int(request.args.get('seed', 0) or 0)
	if not (src and dst):
		abort(400)
	nodes = pathcalc.graph.nodes()
	if seed:
		random.seed(seed)
	else:
		seed = random.randint(1, 2**30) #int(time.time())
		random.seed(seed)
	extra = {
		'u' : __random_dist(nodes, urdist, ua, ub, um),
		'd' : __random_dist(nodes, drdist, ua, ub, 0.0),
	}
	all_paths = pathcalc.compute_paths(src, dst, orderby, loss, mss, c, extra)
	if nth >= len(all_paths):
		abort(400)
	best_path, best_n = all_paths[nth]
	best_edges = gpce.edgefy(best_path)
	in_path = lambda s,d: ((s,d) in best_edges or (d,s) in best_edges)
	g = pathcalc.graph
	nodes = g.nodes()
	edges = g.edges()
	mk_node = lambda n: dict(
		name=n,
		inpath=n in best_path,
		group=nodes.index(n),
		**g.node[n])
	mk_link = lambda s, d: dict(
		source=nodes.index(s),
		target=nodes.index(d),
#		inpath=in_path(s, d),
		inpath=(s,d) in best_edges or (d,s) in best_edges,
		**g.edge[s][d])
	content = json.dumps({
		'nodes' : [mk_node(n) for n in nodes],
		'links' : [mk_link(s, d) for s,d in edges],
		'metrics' : best_n,
		'paths_count' : len(all_paths),
		'best_paths' : __best_paths(all_paths),
		#'best_paths' : __best_paths(pathcalc, src, dst, loss, mss, c, extra),
		'extra' : extra,
		'seed' : seed,
	})
	return Response(content, mimetype='application/json')

if __name__ == '__main__':
	host  = config.get('host', 'localhost')
	port  = int(config.get('port', 8888))
	debug = bool(config.get('debug', True))
	app.run(host=host, port=port, debug=debug, processes=1)
