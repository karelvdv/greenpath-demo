import networkx as nx
from math import sqrt
from collections import defaultdict
from itertools import combinations

def CarbonEmissions(carbon_emissions):
	"""
	Returns function that computes CO2 emissions in grams per kWh
	based on energy sources.
	"""
	def emissions(sources):
		return sum(carbon_emissions[src] * pct for src, pct in sources)
	return emissions

def Node(name, **attrs):
	return (name, attrs)

def Edge(src, dst, **attrs):
	return (src, dst, attrs)

def mathis_equation(rtt, loss, mss, c):
	return ((mss * 8) / (rtt * .001)) * (c / sqrt(loss))

def edgefy(path):
	return zip(path, path[1::])

def reverse_edges(path):
	return ((y, x) for x, y in path)

def subtract_edges(a, b):
	return set(a) - set(b)

def optimal_rwin(bandwidth, rtt):
	return bandwidth * rtt * 0.001

def calc_rtt(G, edges):
	return sum(G.edge[s][d]['rtt'] for s, d in edges)

def calc_bandwidth(G, edges):
	return min(G[s][d]['bndw'] for s,d in edges)

def calc_throughput(G, edges, loss, mss, c):
	"""metric: gbit/s"""
	if loss > 0.0:
		rtt   = calc_rtt(G, edges)
		bndw  = calc_bandwidth(G, edges)
		return min(bndw, mathis_equation(rtt, loss, mss, c) / 10**9)
	return c

def extra_default():
	return defaultdict(defaultdict(lambda: 1))

def all_pairs_all_paths(g, nodes=None):
	if not nodes:
		nodes = g.nodes()
		nodes.sort()
	pairs = combinations(nodes, 2)
	all_paths = defaultdict(lambda: {})
	for a, b in pairs:
		all_paths[a][b] = list(nx.all_simple_paths(g, a, b))
		all_paths[b][a] = list(nx.all_simple_paths(g, b, a))
	return all_paths

class PathCalculator:
	"""
	Use this to calculate all paths and their metrics from A to B.
	"""
	def __init__(self, graph, energy_mixes, kwh_rates, utilization):
		"""
		graph: NetworkX graph.
		energy_mixes: dictionary of energy production mixes.
		kwh_rates: dictionary of cents/kwh per mix.
		utilization: overall network utilization (0 < u <= 1).
		"""
		self.graph        = graph
		self.energy_mixes = energy_mixes
		self.kwh_rates    = kwh_rates
		self.utilization  = 1. / utilization
		self.metrics      = {
			'gbits'    : (self.path_gbits, True),
			'kwh'      : (self.path_kwh, False),
			'gramsco2' : (self.path_gramsco2, False),
			'usdcents' : (self.path_usdcents, False),
			'hops'     : (self.path_hops, False),
		}
		# get all endpoints
		nodes = self.graph.nodes()
		self.endpoints = [n for n in nodes if self.graph.node[n]['endpoint']]
		self.__simple_paths = defaultdict(lambda: defaultdict(lambda: None))

	def get_metrics(self):
		return self.metrics.keys()

	def __node_power(self, n, extra):
		psta = self.graph.node[n]['power_static']
		pdyn = self.graph.node[n]['power_dynamic']
		pue = self.graph.node[n]['pue']
		d   = extra['d'][n]
		u   = extra['u'][n]
		pwr = psta + pdyn * (1 - d) + pdyn * d * u
		return pwr * pue

	def __node_co2(self, n, extra):
		pwr = self.__node_power(n, extra)
		mix = self.energy_mixes[self.graph.node[n]['region']]
		return pwr * mix

	def __node_price(self, n, extra):
		pwr = self.__node_power(n, extra)
		rate = self.kwh_rates[self.graph.node[n]['region']]
		return pwr * rate

	def path_gbits(self, p, loss, mss, c, extra={}):
		"""metric: gbit/s"""
		edges = edgefy(p)
		return calc_throughput(self.graph, edges, loss, mss, c)

	def path_kwh(self, p, loss, mss, c, extra={}):
		"""metric: kWh"""
		edges         = edgefy(p)
		throughput    = calc_throughput(self.graph, edges, loss, mss, c)
		transfer_time = (1. / 3600.0) / throughput
		return transfer_time * self.utilization *\
			sum(self.__node_power(n, extra) for n in p)

	def path_gramsco2(self, p, loss, mss, c, extra={}):
		"""metric: gr. co2 emissions"""
		edges         = edgefy(p)
		throughput    = calc_throughput(self.graph, edges, loss, mss, c)
		transfer_time = (1. / 3600.0) / throughput
		return transfer_time * self.utilization *\
			sum(self.__node_co2(n, extra) for n in p)

	def path_usdcents(self, p, loss, mss, c, extra={}):
		"""metric: dollar cents"""
		edges         = edgefy(p)
		throughput    = calc_throughput(self.graph, edges, loss, mss, c)
		transfer_time = (1. / 3600.0) / throughput
		return transfer_time * self.utilization *\
			sum(self.__node_price(n, extra) for n in p)

	def path_hops(self, p, loss, mss, c, extra={}):
		return len(p) - 1

	def __path_all_metrics(self, p, loss, mss, c, extra={}):
		return dict( (k, v[0](p, loss, mss, c, extra)) for k,v in self.metrics.iteritems() )

	def all_simple_paths(self, src, dst):
		paths = self.__simple_paths[src][dst]
		if not paths:
			paths = sorted(nx.all_simple_paths(self.graph, src, dst), key=len)
			self.__simple_paths[src][dst] = paths
		return paths

	def compute_paths(self, src, dst, orderby, loss, mss, c, extra={}):
		"""
		Calculate all simple paths from src to dst and corresponding metrics.
		Also orders the paths by the chosen metric.
		src: source node
		dst: destination node
		orderby: metric to order by
		loss: network packet loss percentage
		mss: maximum segment
		c: if loss > 0 mathis multiplier else bandwidth
		"""
		reverse = self.metrics[orderby][1]
		key = lambda p: self.__path_all_metrics(p, loss, mss, c, extra)
		all_paths = self.all_simple_paths(src, dst)
		all_keys  = [key(p) for p in all_paths]
		all_stuff = zip(all_paths, all_keys)
		all_stuff.sort(reverse=reverse, key=lambda p: p[1][orderby])
		return all_stuff

def draw_graph(G, paths, pos=None):
	pos = pos or nx.spring_layout(G, scale=4)
	edges = G.edges()
	for path, width, color, style in paths:
		e = edgefy(path)
		edges = subtract_edges(edges, e)
		edges = subtract_edges(edges, reverse_edges(e))
		nx.draw_networkx_edges(G, pos, edgelist=e,
			width=width, edge_color=color, style=style)
	nx.draw_networkx_edges(G, pos, width=1, edgelist=edges)
	nx.draw_networkx_labels(G, pos, font_weight='bold', font_color='b')

def calc_loss(throughput, rtt, mss, c):
	return (c / ( throughput / ((mss * 8) / (rtt * .001)) ) ) ** 2
