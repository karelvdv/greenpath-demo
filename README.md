# Green Path Computation Demo
Author: Karel van der Veldt  
Licence: ISC

This demo was developed in cooperation with the University of Amsterdam, SURFnet, and ESNet. It was presented at the SuperComputing 2013 and TNC 2014 conferences.

## How to run
$ SC13CONF=./esnet\_config.json python sc13demo